package pl.edu.pwsztar.service.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.chess.RulesOfGame;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.domain.enums.FigureType;
import pl.edu.pwsztar.service.ChessService;

import java.util.Arrays;
import java.util.List;

@Service
public class ChessServiceImpl implements ChessService {

    private RulesOfGame bishop;
    private RulesOfGame knight;
    private RulesOfGame king;
    private RulesOfGame queen;
    private RulesOfGame rock;
    private RulesOfGame pawn;

    @Autowired
    public ChessServiceImpl(@Qualifier("Bishop") RulesOfGame bishop,
                            @Qualifier("Knight") RulesOfGame knight,
                            @Qualifier("King") RulesOfGame king,
                            @Qualifier("Queen") RulesOfGame queen,
                            @Qualifier("Rock") RulesOfGame rock,
                            @Qualifier("Pawn") RulesOfGame pawn) {
        this.bishop = bishop;
        this.knight = knight;
        this.king = king;
        this.queen = queen;
        this.rock = rock;
        this.pawn = pawn;
    }

    @Override
    public boolean isCorrectMove(FigureMoveDto figureMoveDto) {
        int xStart;
        int yStart;
        int xDestination;
        int yDestination;

        List<String> startPosition = Arrays.asList(figureMoveDto.getStart().split("_"));
        List<String> endPosition = Arrays.asList(figureMoveDto.getDestination().split("_"));

        xStart = Integer.parseInt(startPosition.get(1));
        xDestination = Integer.parseInt(endPosition.get(1));

        yStart = convertToNumber(startPosition.get(0));
        yDestination = convertToNumber(endPosition.get(0));

        if(figureMoveDto.getType() == FigureType.BISHOP){
            return bishop.isCorrectMove(xStart, yStart, xDestination, yDestination);
        }
        else if(figureMoveDto.getType() == FigureType.KNIGHT){
            return knight.isCorrectMove(xStart, yStart, xDestination, yDestination);
        }
        else if(figureMoveDto.getType() == FigureType.KING) {
            return king.isCorrectMove(xStart, yStart, xDestination, yDestination);
        }
        else if(figureMoveDto.getType() == FigureType.QUEEN) {
            return queen.isCorrectMove(xStart, yStart, xDestination, yDestination);
        }
        else if(figureMoveDto.getType() == FigureType.ROCK) {
            return rock.isCorrectMove(xStart, yStart, xDestination, yDestination);
        }
        else if(figureMoveDto.getType() == FigureType.PAWN) {
            return pawn.isCorrectMove(xStart, yStart, xDestination, yDestination);
        }
        else {
            return false;
        }
    }
    private int convertToNumber(String key) {
        char znak=key.charAt(0);
        int kod_ascii;
        kod_ascii = (int)znak;
        return kod_ascii-96;
    }

}
