package pl.edu.pwsztar.domain.chess;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

public interface RulesOfGame {

    /**
     * Metoda zwraca true, tylko gdy przejscie z polozenia
     * (xStart, yStart) na (xEnd, yEnd) w jednym ruchu jest zgodne
     * z zasadami gry w szachy
     */
    boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd);

    @Component
    @Qualifier("Bishop")
    class Bishop implements RulesOfGame {

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if(xStart == xEnd && yStart == yEnd) {
                return false;
            }

            return Math.abs(xEnd - xStart) == Math.abs(yEnd - yStart);
        }
    }

    @Component
    @Qualifier("Knight")
    class Knight implements RulesOfGame {
        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if(xStart == xEnd && yStart == yEnd) {
                return false;
            }
            return (Math.abs(xEnd-xStart)==1 && Math.abs(yEnd-yStart)==2) || (Math.abs(xEnd-xStart)==2 && Math.abs(yEnd-yStart)==1);
        }
    }

    @Component
    @Qualifier("King")
    class King implements RulesOfGame {
        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if(xStart == xEnd && yStart == yEnd) {
                return false;
            }
            return Math.abs(xEnd-xStart)<=1 && Math.abs(yEnd-yStart)<=1;
        }
    }

    @Component
    @Qualifier("Queen")
    class Queen implements RulesOfGame {
        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if(xStart == xEnd && yStart == yEnd) {
                return false;
            }
            return (Math.abs(xEnd - xStart) == Math.abs(yEnd - yStart)) || (xEnd==xStart || yEnd==yStart);
        }
    }

    @Component
    @Qualifier("Rock")
    class Rock implements RulesOfGame {
        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if(xStart == xEnd && yStart == yEnd) {
                return false;
            }
            return xEnd==xStart || yEnd==yStart;
        }
    }

    @Component
    @Qualifier("Pawn")
    class Pawn implements RulesOfGame {
        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if(xStart == xEnd && yStart == yEnd) {
                return false;
            }
            return ((xEnd == xStart+1) && (yEnd == yStart)) ^ ((xEnd == xStart+2) && (yEnd == yStart));
            //return (Math.abs(yStart-yEnd) == 0 && Math.abs(xStart-xEnd) == 1) || (Math.abs(yStart-yEnd) == 0 && Math.abs(xStart-xEnd) == 2) || (Math.abs(yStart-yEnd) == 1 && Math.abs(xStart-xEnd) == 1);
        }
    }
}
